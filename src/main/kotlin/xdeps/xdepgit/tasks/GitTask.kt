package xdeps.xdepgit.tasks

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.dircache.DirCache
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Classe de service responsável por realizar as operações com o git.
 */
@Component
class GitTask {

    private val workspacePath = Paths.get(System.getProperty("user.dir"))

    /**
     * Inicializa o diretório atual com o git.
     */
    fun init(path: Path = workspacePath): Git {
        return Git.init().setDirectory(path.toFile()).call()
    }

    /**
     * Abre o repositório git para operações.
     */
    fun open(path: Path = workspacePath): Git {
        return Git.open(path.toFile())
    }

    /**
     * Realiza a operação add em um repositório git.
     */
    fun add(git: Git = open(), pattern: String = "."): DirCache? {
        val addCommand = git.add()
        addCommand.addFilepattern(pattern)
        return addCommand.call()
    }
}
